const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	createdOn :{
		type : Date,
		default : new Date()
	},
	carts: [
	    {
	      productId: {
	        type: mongoose.Schema.Types.ObjectId,
	        ref: 'Product', // Reference to the Product model
	        required: [true, "Product ID is required"]
	      },
	      productName: {
	        type: String
	      },
	      addedOn: {
	        type: Date,
	        default: new Date()
	      },
	      status: {
	        type: String,
	        default: "Added to Cart"
	      }
	    }
	  ],
	  orders: [
	    {
	      productId: {
	        type: mongoose.Schema.Types.ObjectId,
	        ref: 'Product', // Reference to the Product model
	        required: [true, "Product ID is required"]
	      },
	      productName: {
	        type: String
	      },
	      ordersOn: {
	        type: Date,
	        default: new Date()
	      },
	      status: {
	        type: String,
	        default: "Ordered"
	      }
	    }
	  ]
	});

	module.exports = mongoose.model("User", userSchema);