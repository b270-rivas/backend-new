const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "Stocks is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,

		// The "new Date()" expression instantiates a new "date" that stores the current date and time whenever a product is created in our database
		default: new Date()
	},
	carts: [
	    {
	      userId: {
	        type: mongoose.Schema.Types.ObjectId,
	        ref: 'User', // Reference to the User model
	        required: [true, "User Id is required"]
	      },
	      userEmail: {
	        type: String
	      },
	      isPaid: {
	        type: Boolean,
	        default: false
	      },
	      dateAdded: {
	        type: Date,
	        default: new Date()
	      }
	    }
	  ],

	  orders: [
	    {
	      userId: {
	        type: mongoose.Schema.Types.ObjectId,
	        ref: 'User', // Reference to the User model
	        required: [true, "User Id is required"]
	      },
	      userEmail: {
	        type: String
	      },
	      isPaid: {
	        type: Boolean,
	        default: true
	      },
	      dateOrdered: {
	        type: Date,
	        default: new Date()
	      }
	    }
	  ]
	});

	module.exports = mongoose.model("Product", productSchema);