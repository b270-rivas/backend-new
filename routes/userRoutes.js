const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller to communicate with our database
router.post("/checkEmail", userController.checkEmailExists);

// Route for user registration
router.post("/register", userController.registerUser);

// Route for user authentication
router.post("/login", userController.loginUser);

// Route for getting a specific user's details
// The "auth.verify" acts as a middleware to ensure that the user is logged in before they can get the user details
router.get("/details", auth.verify, userController.getProfile);

// Route for ordering a user to a product
router.post("/order", auth.verify, userController.order);

// Route for retrieving orders of a user
router.get("/myOrders", auth.verify, userController.myOrders);

// Route for retrieving orders
router.get("/allOrders", auth.verify, userController.allOrders);

// Route for adding a product to cart
router.post("/cart", auth.verify, userController.cart);

// Route for retrieving cart products
router.get("/allCarts", auth.verify, userController.allCarts);

// Route for removing products from cart
router.put("/remove", auth.verify, userController.remove);


module.exports = router;