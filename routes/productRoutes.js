const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product
// router.post("/add", productController.addproduct);
router.post("/add", auth.verify, productController.addProduct);

// Route for retrieving all products
router.get("/all", auth.verify, productController.getAllProducts);

// Route for retrieving all ACTIVE products
router.get("/active", productController.getAllActive);

// Route for retrieving a specific product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:productId", productController.getProduct);

// Route for updating a product
router.put("/:productId/update", auth.verify, productController.updateProduct);

// Route to archiving a product
// A "PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the products from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.patch("/:productId/archive", auth.verify, productController.archiveProduct);

module.exports = router;