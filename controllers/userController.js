const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend application based on the result of the "find" method
*/
module.exports.checkEmailExists = (req, res) => {
	return User.find({email: req.body.email}).then(result => {

		// The "find" method return a record if a match is found
		if(result.length > 0) {
			return res.send (true);

		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return res.send (false);
		}
	})
	.catch(error => res.send (error))
}


// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (req, res) => {

	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,

		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, saltRounds)
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNo: req.body.mobileNo
	})

	// Saves the created object to our database
	return newUser.save().then(user => {
		console.log(user);
		res.send(true)
	})
	.catch(error => {
		console.log(error);
		res.send(false);
	})
}

// User authentication
/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/
module.exports.loginUser = (req, res) => {

	return User.findOne({email: req.body.email}).then(result => {

		// User does not exist
		if(result == null) {

			return res.send({message: "No user found"});

		// User exists
		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {

				// Generates an access token by invoking the "createAccessToken" in the auth.js file
				return res.send({accessToken: auth.createAccessToken(result)});

			// Passwords do not match
			} else {
				return res.send(false)
			}
		}
	})
};

// Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the result document to an empty string("").
	3. Return the result back to the frontend
*/


module.exports.getProfile = (req, res) => {
	
	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	return User.findById(userData.id).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return res.send(result);
	});

};

// Order user to a product
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the product ID to the user's order array
	3. Update the document in the MongoDB Database

	Workflow:
	1. User logs in, server will respond with JWT on successful authentication
	2. A POST request with the JWT in its header and the product ID in its request body will be sent to the /users/order endpoint
	3. Server validates JWT
		- If valid, user will be ordered in the product
		- If invalid, deny request
*/

module.exports.order = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (!userData.isAdmin) {
    try {
      // To retrieve the product name
      const product = await Product.findById(req.body.productId);
      const productName = product.name;

      let data = {
        // User ID and email will be retrieved from the payload/token
        userId: userData.id,
        userEmail: userData.email,
        // product ID will be retrieved from the request body
        productId: req.body.productId,
        productName: productName,
      };
      console.log(data);

      // Add the product ID in the order array of the user
      const isUserUpdated = await User.findByIdAndUpdate(
        data.userId,
        {
          $push: {
            orders: {
              productId: data.productId,
              productName: data.productName,
            },
          },
        },
        { new: true }
      );

      console.log(isUserUpdated);

      // Add the user ID in the order array of the product
      const isProductUpdated = await Product.findByIdAndUpdate(
        data.productId,
        {
          $push: {
            orders: {
              userId: data.userId,
              userEmail: data.userEmail,
            },
          },
          $inc: {
            stocks: -1,
          },
        },
        { new: true }
      );

      console.log(isProductUpdated);

      // Check if the user and product documents have been updated
      if (isUserUpdated && isProductUpdated) {
        return res.send(true);
      } else {
        return res.send(false);
      }
    } catch (error) {
      console.log(error);
      return res.send(false);
    }
  } else {
    return res.send(false);
  }
};

// Retrive orders of a user
module.exports.myOrders = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  // Check if the user is not an admin
  if (!userData.isAdmin) {
    try {
      // Retrieve the user based on the token
      const user = await User.findOne({ _id: userData.id });

      // Retrieve all carts for the user
      const orders = user.orders;

      // Return the orders as a response
      return res.json(orders);
    } catch (error) {
      console.error(error);
      return res.status(500).send("Internal Server Error");
    }
  } else {
    return res.status(403).send("Forbidden");
  }
};

// Retrieve all orders
module.exports.allOrders = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  // Check if the user is an admin
  if (userData.isAdmin) {
    try {
      // Retrieve all users
      const users = await User.find();

      // Retrieve all orders for each user using map and flatten the array
      const orders = [].concat(...users.map(user => user.orders));

      // Return the orders as a response
      return res.json(orders);
    } catch (error) {
      console.error(error);
      return res.status(500).send("Internal Server Error");
    }
  } else {
    return res.status(403).send("Forbidden");
  }
};



// Add to cart
module.exports.cart = async (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (!userData.isAdmin) {
	  try {
	    // To retrieve the product name
	    const product = await Product.findById(req.body.productId);
	    const productName = product.name;

		let data = {
		  // User ID and email will be retrieved from the payload/token
		  userId: userData.id,
		  userEmail: userData.email,
		  // product ID will be retrieved from the request body
		  productId: req.body.productId,
		  productName: productName,
		};
		console.log(data);

		// Add the product ID in the order array of the user
		const isUserUpdated = await User.findByIdAndUpdate(
		  data.userId,
		  {
		    $push: {
		      carts: {
		        productId: data.productId,
		        productName: data.productName,
		      },
		    },
		  },
		  { new: true }
		);

		console.log(isUserUpdated);

		// Add the user ID in the carts array of the product
      const isProductUpdated = await Product.findByIdAndUpdate(
        data.productId,
        {
          $push: {
            carts: {
              userId: data.userId,
              userEmail: data.userEmail,
            },
          },
          $inc: {
            stocks: -1,
          },
        },
        { new: true }
      );

      console.log(isProductUpdated);

      // Check if the user and product documents have been updated
      if (isUserUpdated && isProductUpdated) {
        return res.send(true);
      } else {
        return res.send(false);
      }
    } catch (error) {
      console.log(error);
      return res.send(false);
    }
  } else {
    return res.send(false);
  }
};


// Retrieve all cart
module.exports.allCarts = async (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  // Check if the user is not an admin
  if (!userData.isAdmin) {
    try {
      // Retrieve the user based on the token
      const user = await User.findOne({ _id: userData.id });

      // Retrieve all carts for the user
      const carts = user.carts;

      // Return the products as a response
      return res.json(carts);
    } catch (error) {
      console.error(error);
      return res.status(500).send("Internal Server Error");
    }
  } else {
    return res.status(403).send("Forbidden");
  }
};


// Removing products from cart
module.exports.remove = async (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (!userData.isAdmin) {
	  try {
	    // To retrieve the product name
	    const product = await Product.findById(req.body.productId);
	    const productName = product.name;

		let data = {
		  // User ID and email will be retrieved from the payload/token
		  userId: userData.id,
		  userEmail: userData.email,
		  // product ID will be retrieved from the request body
		  productId: req.body.productId,
		  productName: productName,
		};
		console.log(data);

		// Add the product ID in the order array of the user
		const isUserUpdated = await User.findByIdAndUpdate(
		  data.userId,
		  {
		    $pull: {
		      carts: {
		        productId: data.productId,
		        productName: data.productName,
		      },
		    },
		  },
		  { new: true }
		);

		console.log(isUserUpdated);

		// Add the user ID in the carts array of the product
      const isProductUpdated = await Product.findByIdAndUpdate(
        data.productId,
        {
          $pull: {
            carts: {
              userId: data.userId,
              userEmail: data.userEmail,
            },
          },
          $inc: {
            stocks: -1,
          },
        },
        { new: true }
      );

      console.log(isProductUpdated);

      // Check if the user and product documents have been updated
      if (isUserUpdated && isProductUpdated) {
        return res.send(true);
      } else {
        return res.send(false);
      }
    } catch (error) {
      console.log(error);
      return res.send(false);
    }
  } else {
    return res.send(false);
  }
};