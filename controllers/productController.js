const Product = require("../models/Product");
const auth = require("../auth");
// Create a new product
/*
	Steps:
		1. Create a conditional statement that will check if the user is an administrator.
		2. Create a new product object using the mongoose model and the information from the request body
		3. Save the new product to the database
*/

module.exports.addProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	if(userData.isAdmin) {
		// Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		});

		// Saves the created object to our database
		return newProduct.save()
		// Product creation is successful
		.then(product => {
			console.log(product);
			res.send(true);
		})
		// Product creation failed
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
	}
	
}

// Retrieve all products
/*
	Step:
	1. Retrieve all the products from the database
*/
module.exports.getAllProducts = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		return Product.find({}).then(result => res.send(result));
	} else {
		return res.send(false);
	}
	
}


// Retrieve all ACTIVE products
/*
	Step:
	1. Retrieve all the products from the database with property of "isActive: true"
*/
module.exports.getAllActive = (req, res) => {

	return Product.find({isActive: true}).then(result => res.send(result));
}


// Retrieving a specific product
/*
	Steps:
	1. Retrieve the product that matches the product ID provided in the URL
*/
module.exports.getProduct = (req, res) => {

	console.log(req.params.productId)

	return Product.findById(req.params.productId)
	.then(result => {
		console.log(result);
		return res.send(result)
	})
	.catch(error => {
		console.log(error);
		return res.send(error);
	})
}


// Update a product
/*
	Steps:
	1. Create a variable "updatedProduct" which will contain the information retrieved from the request body
	2. Find and update the product using the product ID retrieved from the request params property and the variable "updatedProduct" containing the information from the request body
*/
// Information to update a product will be coming from both the URL parameters and the request body
module.exports.updateProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}

		// Syntax: findByIdAndUpdate(documentId, updatesToBeApplied)
		// {new:true} - returns the updated document
		return Product.findByIdAndUpdate(req.params.productId, updateProduct, {new:true})
		.then(result => {
			console.log(result);
			res.send(result);
		})
		.catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		return res.send(false);
		// return res.send("You don't have access to this page!")
	}
}


// Archive/Unarchive a product
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the product "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active products are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {

		let updateActiveField = {
			isActive : req.body.isActive
		};

		return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
		.then(result => {
			console.log(result);
			// The product status is successfully updated
			return res.send(true)
		})
		.catch(error =>{
			console.log(error);
			// The product update failed
			return res.send(false);
		})

	} else {
		return res.send(false);
	}

	
};